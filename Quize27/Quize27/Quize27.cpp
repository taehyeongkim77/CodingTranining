// Quize27.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <deque>
#include <string>


using namespace System;
using namespace std;

string standardize(const char * input)
{
	int i = 0;
	string result;

	deque<string> segment;
	string s_temp_segement;

	while(input[i] != '\0')
	{
		if(i == 0)
		{
			i++;
			continue;
		}

		if(input[i] == '/')
		{
			if(!s_temp_segement.empty())
			{
				segment.push_back(s_temp_segement);
				s_temp_segement.clear();
			}
		}
		else
		{
			bool check_1 = input[i-1] == '.';
			bool check_2 = input[i] == '.';

			if(check_1 && check_2)
			{
				segment.pop_back();
			}
			else if(check_2)
			{
				// do nothing
			}
			else
			{
				s_temp_segement += input[i];
			}
		}

		i++;
	}

	while (!segment.empty())
	{
		result += '/';
		result += segment.front();
		segment.pop_front();
	}

	result += '/';

	return result;
}

int main(array<System::String ^> ^args)
{
    const char * s_input = "/usr/./bin/./test/../";
	string result;


	result = standardize(s_input);

    return 0;
}
