// Quize38.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"

#define ARRAY_SIZE 9

using namespace System;

int findMissingNum(int input[])
{
	int result, size, total_sum, array_sum;

	size = ARRAY_SIZE + 1;

	total_sum = size * (size + 1) / 2;

	for(int i = 0; i < ARRAY_SIZE; i++)
	{
		array_sum += input[i];
	}

	result = total_sum - array_sum;
	
	return result;
}

int main(array<System::String ^> ^args)
{
    //Console::WriteLine(L"Hello World");
	int input[ARRAY_SIZE] = {1, 2, 3, 4, 5, 6, 8, 9, 10};
	int result;

	result = findMissingNum(input);
    return 0;
}
