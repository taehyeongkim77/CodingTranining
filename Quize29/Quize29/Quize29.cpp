// Quize29.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <string>
#include <map>
#include <deque>

using namespace System;
using namespace std;



const char * rearrange(const char * c_str)
{
	string s_result;
	const char * result;
	const char * Error_return = " ";
	int i = 0;
	map<char, int> frequencies;
	deque<pair<char, int>> dq_count;


	map<char, int>::iterator it;

	for(;i < strlen(c_str); i++)				// strlen 하면 6 출력 -> 5까지 for문 돌려야 함.
	{
		it = frequencies.find(c_str[i]);
		if(it != frequencies.end())
		{
			it->second++;
		}
		else
		{
			frequencies.insert(pair<char, int>(c_str[i], 1));
		}
	}

	int max_val =0;
	char max_char;
	int size = frequencies.size();

	it = frequencies.begin();
	while(it != frequencies.end())
	{
		map<char, int>::iterator it2;
		it2 = frequencies.begin();

		while(it2 != frequencies.end())
		{
			int max_val_temp = it2->second;

			if(max_val < max_val_temp)
			{
				max_val = max_val_temp;
				max_char = it2->first;
			}

			it2++;
		}

		
		if(it->second > 0)
		{
			dq_count.push_back(pair<char, int>(it->first, it->second));
			it->second--;
		}
		else
		{
			frequencies.erase(frequencies.find(max_char));
		}

		it++;
	}

	char ch_temp = '\0';
	while(!dq_count.empty())
	{
		pair<char, int> pop_val;
		pop_val = dq_count.front();
		dq_count.pop_front();
		s_result += pop_val.first;


		pair<char, int> insert_val;
		insert_val = pair<char, int>(pop_val.first, pop_val.second-1);

		if((pop_val.second-1) > 0)
		{
			if(ch_temp == insert_val.first)
			{
				return Error_return;
			}
			dq_count.push_back(insert_val);
			ch_temp = insert_val.first;
		}
		else
		{
			// Error
		}
	}

	result = s_result.c_str();

	return result;
}



int main(array<System::String ^> ^args)
{
	const char * input = "cbbbaa";
	const char * result = rearrange(input);

	printf("%s\n", result);

    return 0;
}

