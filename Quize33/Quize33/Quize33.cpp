// Quize33.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <stack>

using namespace System;
using namespace std;

template <typename T>
class MyQueue {
private:
	stack<T> first;
	stack<T> second;

	void put(T data){
		first.push(data);
	}

	T pop(){
		if(!second.empty())
		{
			while(!first.empty())
			{
				second.push(first.top());
				first.pop();
			}


			T return_val = second.top();
			second.pop();

			return return_val;
		}
		else
		{
			return NULL;
		}
	}

};

int main(array<System::String ^> ^args)
{
    
    return 0;
}
