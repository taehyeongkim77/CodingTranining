// Qsort.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"

#define SWAP(x, y, temp) ( (temp)=(x), (x)=(y), (y)=(temp) )


using namespace System;

template <typename T>
T partition(T list[], int left, int right) {
	int pivot, temp;
	int i, j;

	i = left;
	j = right + 1;
	pivot = list[left];

	while(i < j) {
		if (i<=j && list[i]<pivot) {
			i++;
		}
		if (j>=i && list[j]>pivot) {
			j--;
		}
		if(i<j) {
			SWAP(list[i], list[j], temp);
		}
	}

	SWAP(list[left], list[j], temp);

	return j;
}

template <typename T>
void quicksort(T list[], int left, int right) {
	int pivot = 0;

	if(left < right) {
		pivot = partition(list, left, right);
		quicksort<T>(list, left, pivot-1);
		quicksort<T>(list, pivot+1, right);
	}
}

int main(array<System::String ^> ^args)
{
    //Console::WriteLine(L"Hello World");
    return 0;
}
