// Quize35.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <vector>

using namespace System;
using namespace std;

template <typename T>
T fixed_point(vector<T> input)
{
	int low, high, mid = 0;

	high = input.size();	// 4

	while(low <= high)
	{
		mid = (low+high) / 2;

		if(input.at(mid) == mid)
		{
			return mid;
		}
		else
		{
			if(input.at(mid) < mid)
			{
				low = mid + 1;
			}
			else
			{
				high = mid - 1;
			}
		}
	}

	return 0;
}

int main(array<System::String ^> ^args)
{
	int result = 0;
	vector<int> input_val;

	input_val.push_back(-30);
	input_val.push_back(1);
	input_val.push_back(4);
	input_val.push_back(60);

	/*
	// 다음과 같이 선언해도 됨.
	int myints[] = {-30, 1, 4, 60};
	vector<int> input_val2(myints, myints+sizeof(myints)/sizeof(int));
	*/

    result = fixed_point<int>(input_val);

    return 0;
}
