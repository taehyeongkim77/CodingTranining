// Radix_sort.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"

using namespace System;

void radix_sort(int arr[], int size)
{
	int i, j, k, a, r;
	int *que[10], top[10];

	// 사용할 큐를 초기화
	for(i = 0; i < 10; i++)
	{
		que[i] = new int[size];
		top[i] = -1;
	}

	// 최대값을 찾아서 몇자리 수인지 센다
	j = arr[0];
	for(i = 0; i < size; i++)
	{
		if(arr[i] > j)
		{
			// 제일 큰 자리가 j가 되겠지?
			j = arr[i];
		}
	}

	r = 1;
	while(r <= j)
	{
		// 몇 번째 자리수 까지 있는지 r에 계산
		r *= 10;
	}

	for(i = 1; i <= r; i *= 10)
	{
		for(j = 0; j < size; j++)
		{
			k = (arr[j] / i) % (i * 10);	// 해당 자리수의 큐에 데이터 저장
			que[k][++top[k]] = arr[j];
		}
		// 자릿수 순서대로 큐의 데이터를 꺼냄
		a = 0;
		for(j = 0; j < 10; j++)
		{
			for(k = 0; k <= top[j]; k++)
			{
				arr[a++] = que[j][k];
			}
			top[j] = -1;
		}
	}

	// 메모리 반환
	for(i = 0; i < 10; i++)
	{
		delete que[i];
	}
}

// 큐를 사용하지 않고, 자릿수 숫자를 세는 방식
void radix_sort2(int arr[], int size)
{
	int i, j, r, count[10];
	int *a, *b, *t;
	a = arr;
	// 임시 공간을 초기화
	b = new int[size];
	
	// 최대값을 찾아서 몇 자리 수인지 센다
	for(i = 1; i < size; i++)
	{
		if(arr[i] > j)
		{
			j = arr[i];
		}
	}
	r = 1;
	while(r < j)
	{
		r *= 10;
	}

	for(i = 1; i <= r; i *= 10)
	{
		for(j = 0; j < 10; j++)
		{
			// count 초기화
			count[j] = 0;
		}
		for(j = 0; j < size; j++)
		{
			// 각 자리수(0~9) 별로 몇 개씩 있는지 카운트
			count[(a[j] / i) * 10]++;
		}
		for(j = 1; j < 10; j++)
		{	
			// 누적 개수를 카운트
			count[j] += count[j - 1];
		}
		for(j = size - 1; j >= 0; j--)
		{
			// 원소 배치
			b[--count[(a[j] / i) % 10]] = a[j];
		}
		t = a;
		a = b;
		b = t;
	}

	if(a != arr)
	{
		for(i = 0; i < size; i++)
		{
			arr[i] = a[i];
		}
		delete a;
	}
	else
	{
		delete b;
	}
}

int main(array<System::String ^> ^args)
{
    //Console::WriteLine(L"Hello World");
    return 0;
}
