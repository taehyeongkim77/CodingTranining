// Quize36.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"

using namespace System;

bool isPowerofFour(int n)
{
	if(n == 0)
	{
		return false;
	}
	else
	{
		while(n >= 1)
		{
			if(n%4 == 0)
			{
				return true;
			}
			else
			{
				n /= 4;
			}
		}
	}

	return false;
}

unsigned char Ten2Two_uc2i(int num)
{
	// 10진수 = 13 이라면 13 % 2 = 1 -> 6 % 2 = 0 -> 3 % 2 = 1 -> 1 
	// 거꾸로 읽으면 1101
	unsigned char result;

	for(int i = 0; i < 8; i++)
	{
		if((0x80 >> i) & num)
		{
			result |= (0x80 >> i);
		}
	}

	return result;
}


bool isPowerofFour2(int n)
{
	unsigned char c_n;

	c_n = Ten2Two_uc2i(n);

	if(c_n && 1)
	{
		return false;
	}
	else
	{
		for(int i = 1; i <= 8; i++)
		{
			unsigned char uc_compare = 1 << i;

			if(c_n && uc_compare)
			{
				return true;
			}
			else
			{
				// nothing
			}
		}
	}

	return false;
}

int main(array<System::String ^> ^args)
{
	bool result;

	result = isPowerofFour(15);
	result = isPowerofFour2(15);

    return 0;
}
