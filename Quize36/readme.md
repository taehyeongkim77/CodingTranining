Quize 36.
===


## Q. 주어진 정수가 4의 거듭제곱인지 확인하시오.

---

하지만 이보다 더 시간복잡도를 효율적으로 할 수 있습니다. 주어진 정수를 이진법으로 나타내였을때, 4의 거듭제곱이 되려면 만족해야하는 두가지 조건이 있습니다.<br>
1) 이진법으로 나타냈을때 1이 하나여야 합니다.<br>
2) 1 뒤로 0의 갯수가 짝수여야 합니다.<br>

10 = 2 = false<br>
100 = 4 = true<br>
1000 = 8 = false<br>
10000 = 16 = true<br>

```c

#include "stdafx.h"

using namespace System;

bool isPowerofFour(int n)
{
	if(n == 0)
	{
		return false;
	}
	else
	{
		while(n >= 1)
		{
			if(n%4 == 0)
			{
				return true;
			}
			else
			{
				n /= 4;
			}
		}
	}

	return false;
}

unsigned char Ten2Two_uc2i(int num)
{
	// 10진수 = 13 이라면 13 % 2 = 1 -> 6 % 2 = 0 -> 3 % 2 = 1 -> 1 
	// 거꾸로 읽으면 1101
	unsigned char result;

	for(int i = 0; i < 8; i++)
	{
		if((0x80 >> i) & num)
		{
			result |= (0x80 >> i);
		}
	}

	return result;
}


bool isPowerofFour2(int n)
{
	unsigned char c_n;

	c_n = Ten2Two_uc2i(n);

	if(c_n && 1)
	{
		return false;
	}
	else
	{
		for(int i = 1; i <= 8; i++)
		{
			unsigned char uc_compare = 1 << i;

			if(c_n && uc_compare)
			{
				return true;
			}
			else
			{
				// nothing
			}
		}
	}

	return false;
}

int main(array<System::String ^> ^args)
{
	bool result;

	result = isPowerofFour(15);
	result = isPowerofFour2(15);

    return 0;
}


```