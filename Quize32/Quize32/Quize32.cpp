// Quize32.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"

#include <stack>

#include "BinaryTree.h"


using namespace System;
using namespace std;

template <typename T>
void solution(Tree<T> root);


int main(array<System::String ^> ^args)
{
	// Tree 생성
	Tree<int> tree(1);
	tree.insertNode(new TreeNode<int>(2));
	tree.insertNode(new TreeNode<int>(3));
	tree.insertNode(new TreeNode<int>(4));
	tree.insertNode(new TreeNode<int>(5));
	tree.insertNode(new TreeNode<int>(6));
	tree.insertNode(new TreeNode<int>(7));

	solution<int>(tree);


    return 0;
}

template <typename T>
void solution(Tree<T> root)
{
	stack<TreeNode<T>*> forward;
	stack<TreeNode<T>*> backward;
	TreeNode<T>* tmp_tree = NULL;

	forward.push(root.getRoot());

	while(!forward.empty() || !backward.empty())
	{
		while(!forward.empty())
		{
			tmp_tree = forward.top();
			forward.pop();

			cout << tmp_tree->getData() << ", ";

			if(tmp_tree->getLeftNode() != NULL)
			{
				backward.push(tmp_tree->getLeftNode());
			}

			if(tmp_tree->getRigtNode() != NULL)
			{
				backward.push(tmp_tree->getRigtNode());
			}
		}

		while (!backward.empty())
		{
			tmp_tree = backward.top();
			backward.pop();


			cout << tmp_tree->getData() << ", ";


			if(tmp_tree->getRigtNode() != NULL)
			{
				forward.push(tmp_tree->getRigtNode());
			}

			if(tmp_tree->getLeftNode() != NULL)
			{
				forward.push(tmp_tree->getLeftNode());
			}
		}
	}
}
