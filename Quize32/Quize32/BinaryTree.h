#include <iostream>

using namespace std;


template <typename T>
class Tree;

template <typename T>
class TreeNode {
	friend class Tree<T>;
private:
	T data;
	TreeNode* left;
	TreeNode* right;
public:
	TreeNode(T data = 0, TreeNode* left = NULL, TreeNode* right = NULL) {
		this->data = data;
		this->left = left;
		this->right = right;
	}
	TreeNode operator=(const TreeNode& rhs) {
		this->data = rhs.data;
		this->left = rhs.left;
		this->right = rhs.right;
		return this;
	}
	T getData() {
		return data;
	}
	TreeNode* getLeftNode() {
		return left;
	}
	TreeNode* getRigtNode() {
		return right;
	}
};

template <typename T>
class Tree{
private:
	TreeNode<T>* root;
public:
	Tree(T data = 0) {
		root = new TreeNode<T>(data);
	}

	void buildTree() {
		// 자동으로 트리를 생성하고 싶으면 여기를 참고.
		/*
		insertNode(new TreeNode<int>(3));
		insertNode(new TreeNode<int>(10));
		insertNode(new TreeNode<int>(14));
		insertNode(new TreeNode<int>(2));
		insertNode(new TreeNode<int>(5));
		insertNode(new TreeNode<int>(11));
		insertNode(new TreeNode<int>(16));
		*/
	}

	void insertNode(TreeNode<T>* node) {
		if(search(root, node->data) == NULL) {
			TreeNode<T>* parent = NULL;
			TreeNode<T>* current = root;

			while (current != NULL) {
				parent = current;
				if(node->data < parent->data) {
					current = current->left;
				}
				else {
					current = current->right;
				}
			}
			if(node->data < parent->data) {
				parent->left = node;
			}
			else {
				parent->right = node;
			}
			cout << "Inserted " << node->data << endl;
		}
	}

	TreeNode<T>* getRoot() {
		return root;
	}

	void visit(TreeNode<T>* current) {
		cout << current->data << " ";
	}

	void preorder(TreeNode<T>* currnt) {
		if(currnt != NULL) {
			visit(currnt);
			preorder(currnt->left);
			preorder(currnt->right);
		}
	}

	void indorder(TreeNode<T>* current) {
		if(current != NULL) {
			indorder(current->left);
			visit(current);
			indorder(current->right);
		}
	}

	void postorder(TreeNode<T>* current) {
		if(current != NULL) {
			postorder(current->left);
			postorder(current->right);
			visit(current);
		}
	}

	TreeNode<T>* search(TreeNode<T>* current, T data) {
		if(current == NULL) return NULL;
		if(data == current->data) {
			return current;
		}
		else if(data < current->data) {
			search(current->left, data);
		}
		else {
			search(current->right, data);
		}
	}
};
