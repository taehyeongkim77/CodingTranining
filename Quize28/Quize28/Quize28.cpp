// Quize28.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <vector>

using namespace System;
using namespace std;

template <typename T>
T solution(vector<T> nums)
{
	// (참고) T smallest_impossible = nums.being(); 불가능 begin은 iterator만 받을 수 있음.
	T smallest_impossible = nums[0];

	for(vector<T>::iterator it = nums.begin(); it != nums.end(); it++)
	{
		if(*it <= smallest_impossible)
		{
			smallest_impossible += *it;
		}
		else
		{
			return smallest_impossible;
		}
	}

	return 0;
}

int main(array<System::String ^> ^args)
{
	int myints[] = {1, 2, 3, 8};
	vector<int> nums(myints, myints + sizeof(myints) / sizeof(int));

	int result;

    result = solution(nums);

    return 0;
}
