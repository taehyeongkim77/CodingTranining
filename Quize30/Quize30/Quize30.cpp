// Quize30.cpp : 기본 프로젝트 파일입니다.

#include "stdafx.h"
#include <math.h>

// unsigned char로 구현
// unsigned char의 경우 1 = 0000 0001
// 2 = 0000 0010
unsigned char Ten2Two_uc2i(int num)
{
	// 10진수 = 13 이라면 13 % 2 = 1 -> 6 % 2 = 0 -> 3 % 2 = 1 -> 1 
	// 거꾸로 읽으면 1101
	unsigned char result;

	for(int i = 0; i < 8; i++)
	{
		if((0x80 >> i) & num)
		{
			result |= (0x80 >> i);
		}
	}

	return result;
}

// 2진수 -> 10진수
// 2진수 = 1101 이라면 (2^3*1)+(2^2*1)+(2^1*0)+(2^0*1) = 10진수가 된다.
int Two2Ten_i2uc(unsigned char num)
{
	int result = 0;

	for(int i = 0; i < 8; i++)
	{
		if((0x80 >> i) & num)
		{
			//result += 2^(7-i);

			result += pow(2.0, 7.0 - i);
		}
	}

	return result;
}

int countSetBits(unsigned char n)
{
	int count = 0;

	while(n)
	{
		n &= (n-1);
		count+=1;
	}

	return count;
}

int main(array<System::String ^> ^args)
{
    unsigned char uc_result;
	int i_result;

	
	uc_result = Ten2Two_uc2i(13);
	i_result = Two2Ten_i2uc(uc_result);

    return 0;
}
