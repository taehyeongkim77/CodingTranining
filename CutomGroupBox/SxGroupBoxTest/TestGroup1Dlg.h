#if !defined(AFX_TESTGROUP1DLG_H__9DA31EA1_C23B_4F1C_AED9_A5925A48D464__INCLUDED_)
#define AFX_TESTGROUP1DLG_H__9DA31EA1_C23B_4F1C_AED9_A5925A48D464__INCLUDED_

#pragma once

#include "SxGroupBox.h"

class CTestGroup1Dlg : public CDialog
{
public:
	CTestGroup1Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CTestGroup1Dlg)
	enum { IDD = IDD_TESTGROUP1_DIALOG };
	CComboBox	Ctrl_Combo4;
	CComboBox	Ctrl_Combo3;
	CComboBox	Ctrl_Combo2;
	CComboBox	Ctrl_Combo1;
	CSxGroupBox	Ctrl_Group_1;
	CSxGroupBox	Ctrl_Group_2;
	CSxGroupBox	Ctrl_Group_3;
	CSxGroupBox	Ctrl_Group_4;
	CButton	Ctrl_Btn_1;
	CButton	Ctrl_Btn_2;
	CButton	Ctrl_Btn_3;
	CButton	Ctrl_Btn_4;
	CButton	Ctrl_Btn_Ok;
	CTime	Ctrl_DateTime1;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestGroup1Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTestGroup1Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnBtnOk();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnSelchangeCombo3();
	afx_msg void OnSelchangeCombo4();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnButton4();
	afx_msg void OnBtnFontColor();
	afx_msg void OnBtnHiliteColor();
	afx_msg void OnBtnLineColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTGROUP1DLG_H__9DA31EA1_C23B_4F1C_AED9_A5925A48D464__INCLUDED_)
