#include "stdafx.h"
#include "TestGroup1.h"
#include "TestGroup1Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//
//==============================================================
// Aboutbox Implementation
//==============================================================
//
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//
//==============================================================
// Demo Dialog Implementation
//==============================================================
//
CTestGroup1Dlg::CTestGroup1Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestGroup1Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestGroup1Dlg)
	Ctrl_DateTime1 = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestGroup1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestGroup1Dlg)
	DDX_Control(pDX, IDC_COMBO4, Ctrl_Combo4);
	DDX_Control(pDX, IDC_COMBO3, Ctrl_Combo3);
	DDX_Control(pDX, IDC_COMBO2, Ctrl_Combo2);
	DDX_Control(pDX, IDC_COMBO1, Ctrl_Combo1);
	DDX_Control(pDX, IDC_GROUP1, Ctrl_Group_1);
	DDX_Control(pDX, IDC_GROUP2, Ctrl_Group_2);
	DDX_Control(pDX, IDC_GROUP3, Ctrl_Group_3);
	DDX_Control(pDX, IDC_GROUP4, Ctrl_Group_4);
	DDX_Control(pDX, IDC_BUTTON1, Ctrl_Btn_1);
	DDX_Control(pDX, IDC_BUTTON2, Ctrl_Btn_2);
	DDX_Control(pDX, IDC_BUTTON3, Ctrl_Btn_3);
	DDX_Control(pDX, IDC_BUTTON4, Ctrl_Btn_4);
	DDX_Control(pDX, IDC_BTN_OK, Ctrl_Btn_Ok);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTestGroup1Dlg, CDialog)
	//{{AFX_MSG_MAP(CTestGroup1Dlg)
	ON_WM_SYSCOMMAND()
	ON_BN_CLICKED(IDC_BTN_OK, OnBtnOk)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnSelchangeCombo3)
	ON_CBN_SELCHANGE(IDC_COMBO4, OnSelchangeCombo4)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BTN_FONT_COLOR, OnBtnFontColor)
	ON_BN_CLICKED(IDC_BTN_HILITE_COLOR, OnBtnHiliteColor)
	ON_BN_CLICKED(IDC_BTN_LINE_COLOR, OnBtnLineColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CTestGroup1Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
//
// create AboutBox menu item
//
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
//
// set the icon for this dialog
//
	SetIcon(m_hIcon, TRUE);   // Set big icon
	SetIcon(m_hIcon, FALSE);  // Set small icon
//
// setup the group controls
//
	// create the fonts to use
	CSxLogFont Arial10b(100,FW_BOLD,false,"Arial");
	CSxLogFont Times12bi(140,FW_BOLD,true,"Times New Roman");
	CSxLogFont Tahoma10(100,FW_SEMIBOLD,false,"Tahoma");

	// set the text fonts
	Ctrl_Group_1.SetFont(&Arial10b);
	Ctrl_Group_3.SetFont(&Times12bi);
	Ctrl_Group_4.SetFont(&Tahoma10);

	// set the text colors
	Ctrl_Group_1.SetTextColor(RGB(0,0,200));
	Ctrl_Group_3.SetTextColor(RGB(0,80,0));
	Ctrl_Group_4.SetTextColor(RGB(100,0,200));

	// set the box colors
	Ctrl_Group_1.SetBoxColors(RGB(0,0,200),RGB(220,240,240));
	Ctrl_Group_3.SetBoxColors(RGB(0,40,0),RGB(0,180,0));
	Ctrl_Group_4.SetBoxColors(RGB(100,0,200));

	// set the box styles
	Ctrl_Group_3.SetLineStyle(BS_FLAT);

	// set the text strings
	Ctrl_Group_3.SetText("Group 3");

	// set the text alignment
	Ctrl_Group_3.SetTextAlign(BS_CENTER);

	OnButton1();
//
// setup the comboboxes
//
	Ctrl_Combo1.ResetContent();
	Ctrl_Combo1.AddString("Enable");
	Ctrl_Combo1.AddString("Disable");
	Ctrl_Combo1.SetCurSel(0);

	Ctrl_Combo2.ResetContent();
	Ctrl_Combo2.AddString("Left");
	Ctrl_Combo2.AddString("Center");
	Ctrl_Combo2.AddString("Right");
	Ctrl_Combo2.SetCurSel(0);

	Ctrl_Combo3.ResetContent();
	Ctrl_Combo3.AddString("Thickness = 1");
	Ctrl_Combo3.AddString("Thickness = 2");
	Ctrl_Combo3.AddString("Thickness = 3");
	Ctrl_Combo3.AddString("Thickness = 4");
	Ctrl_Combo3.AddString("Thickness = 5");
	Ctrl_Combo3.SetCurSel(0);

	Ctrl_Combo4.ResetContent();
	Ctrl_Combo4.AddString("Flat");
	Ctrl_Combo4.AddString("3D");
	Ctrl_Combo4.AddString("Rect");
	Ctrl_Combo4.SetCurSel(0);

	return TRUE;
}
//
// handle the system menu command
//
void CTestGroup1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}
//
// combobox handlers
//
void CTestGroup1Dlg::OnSelchangeCombo1() 
{
	int ii = Ctrl_Combo1.GetCurSel();
	if(ii)
		Ctrl_Group_4.EnableWindow(FALSE);
	else
		Ctrl_Group_4.EnableWindow(TRUE);
	Invalidate();
}
void CTestGroup1Dlg::OnSelchangeCombo2() 
{
	int ii = Ctrl_Combo2.GetCurSel();
	if(ii == 0)
		Ctrl_Group_4.SetTextAlign(BS_LEFT);
	else if(ii == 1)
		Ctrl_Group_4.SetTextAlign(BS_CENTER);
	else if(ii == 2)
		Ctrl_Group_4.SetTextAlign(BS_RIGHT);
	Invalidate();
}
void CTestGroup1Dlg::OnSelchangeCombo3() 
{
	// set line thickness
	int ii = Ctrl_Combo3.GetCurSel();
	Ctrl_Group_3.SetLineThickness(ii+1);
	Invalidate();
}
void CTestGroup1Dlg::OnSelchangeCombo4() 
{
	int ii = Ctrl_Combo4.GetCurSel();
	if(ii == 0)
		Ctrl_Group_3.SetLineStyle(BS_FLAT);
	else if(ii == 1)
		Ctrl_Group_3.SetLineStyle(BS_3D);
	else if(ii == 2)
		Ctrl_Group_3.SetLineStyle(BS_RECT);
	Invalidate();	
}
//
// button handlers
//
void CTestGroup1Dlg::OnButton1() 
{
	CSxLogFont Comic12b(120,FW_BOLD,false,"Comic Sans MS");
	Ctrl_Group_2.SetFont(&Comic12b);
	Ctrl_Group_2.SetTextColor(RGB(200,0,0));
	Ctrl_Group_2.SetBoxColors(RGB(200,0,0));
	Ctrl_Group_2.SetLineThickness(5);
	Ctrl_Group_2.SetLineStyle(BS_FLAT);
	Ctrl_Group_2.SetText("BS_FLAT");
	Invalidate();
}
void CTestGroup1Dlg::OnButton2() 
{
	CSxLogFont ArialR12b(120,FW_NORMAL,false,"Arial Rounded MT Bold");
	Ctrl_Group_2.SetFont(&ArialR12b);
	Ctrl_Group_2.SetTextColor(RGB(200,100,0));
	Ctrl_Group_2.SetBoxColors(RGB(200,100,0),RGB(0,50,200));	
	Ctrl_Group_2.SetLineThickness(5);
	Ctrl_Group_2.SetLineStyle(BS_3D);
	Ctrl_Group_2.SetText("BS_3D");
	Invalidate();
}
void CTestGroup1Dlg::OnButton3() 
{
	CSxLogFont ArialR12b(120,FW_NORMAL,false,"Arial Rounded MT Bold");
	Ctrl_Group_2.SetFont(&ArialR12b);
	Ctrl_Group_2.SetTextColor(RGB(100,0,200));
	Ctrl_Group_2.SetBoxColors(RGB(100,0,200),RGB(200,100,0));	
	Ctrl_Group_2.SetLineThickness(5);
	Ctrl_Group_2.SetLineStyle(BS_RECT);
	Ctrl_Group_2.SetText("BS_RECT");
	Invalidate();	
}
void CTestGroup1Dlg::OnButton4() 
{
	Ctrl_Group_2.SetFont();
	Ctrl_Group_2.SetTextColor(RGB(0,0,0));
	Ctrl_Group_2.SetBoxColors();	
	Ctrl_Group_2.SetLineThickness(1);
	Ctrl_Group_2.SetLineStyle(BS_3D);
	Ctrl_Group_2.SetText("Default");
	Invalidate();
}
void CTestGroup1Dlg::OnBtnOk() 
{
	OnOK();
}
//
// set the font color
//
void CTestGroup1Dlg::OnBtnFontColor() 
{
	CColorDialog dlg( Ctrl_Group_1.GetTextColor() );
	dlg.DoModal();
	Ctrl_Group_1.SetTextColor( dlg.GetColor(), true);
}
//
// set the line color
//
void CTestGroup1Dlg::OnBtnLineColor() 
{
	COLORREF Color1, Color2;
	Ctrl_Group_1.GetBoxColors(Color1, Color2);

	CColorDialog dlg(Color1);
	dlg.DoModal();
	Ctrl_Group_1.SetBoxColors( dlg.GetColor(), Color2, true);
}
//
// set the line 3D hilite color
//
void CTestGroup1Dlg::OnBtnHiliteColor() 
{
	COLORREF Color1, Color2;
	Ctrl_Group_1.GetBoxColors(Color1, Color2);

	CColorDialog dlg(Color2);
	dlg.DoModal();
	Ctrl_Group_1.SetBoxColors( Color1, dlg.GetColor(), true);
}

