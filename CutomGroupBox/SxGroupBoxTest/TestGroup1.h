#if !defined(AFX_TESTGROUP1_H__2D96F285_2FDE_4721_AA36_610C440526F1__INCLUDED_)
#define AFX_TESTGROUP1_H__2D96F285_2FDE_4721_AA36_610C440526F1__INCLUDED_

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

class CTestGroup1App : public CWinApp
{
public:
	CTestGroup1App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestGroup1App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTestGroup1App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTGROUP1_H__2D96F285_2FDE_4721_AA36_610C440526F1__INCLUDED_)
