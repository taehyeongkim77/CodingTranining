//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by TestGroup1.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTGROUP1_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_GROUP1                      1000
#define IDC_GROUP2                      1003
#define IDC_GROUP3                      1004
#define IDC_BUTTON1                     1010
#define IDC_BUTTON2                     1011
#define IDC_BUTTON3                     1012
#define IDC_BUTTON4                     1013
#define IDC_BTN_OK                      1014
#define IDC_GROUP4                      1015
#define IDC_COMBO1                      1016
#define IDC_COMBO2                      1017
#define IDC_COMBO3                      1018
#define IDC_COMBO4                      1019
#define IDC_BTN_FONT_COLOR              1020
#define IDC_BTN_LINE_COLOR              1021
#define IDC_BTN_HILITE_COLOR            1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
